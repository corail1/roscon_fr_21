Corail is a project that aims to allows real-time programming using ROS2 and rclcpp.
First, we will see what we understand by real-time programming and what it implies about the underlying middleware.
Then we will look at ROS2 and see why it is not completely satisfying in particular when we speak about real-time execution.
Finally, I will show Corail which proposes solutions to solve some of the ROS2 problems. Corail relies on rclcpp Executors and uses POSIX features to implements a new one that gives complete control over the scheduling parameters,like priority or affinity. It allows a deterministic execution and so temporal analysis and validation of complex systems made with ROS2.
