// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include <chrono>

#include "rclcpp/rclcpp.hpp"

#include "corail_core/corail_core.hpp"
#include "utils/cpu.hpp"

using namespace std::chrono_literals;

class test_node : public corail_core::RealTimeNode
{
    public :
        test_node(std::string name) : RealTimeNode(name)
        {
            t2_ = this->create_rt_timer("task2", 0, 10, 50ms, std::bind(&test_node::worker2,this));
            t1_ = this->create_rt_timer("task1", 0, 20, 10ms, std::bind(&test_node::worker1,this));
        }
    void worker2()
    {
        utils::burn_cpu(13000);
    }
    void worker1()
    {
        utils::burn_cpu(3000);
    }

    private :
    corail_core::PeriodicTask::SharedPtr t1_;
    corail_core::PeriodicTask::SharedPtr t2_;
};

int main(int argc, char * argv[])
{
    rclcpp::init(argc,argv);

    corail_core::RealTimeExecutor exec("executor");
    auto node = std::make_shared<test_node>("node");
    exec.add_rt_node(node);
    
    exec.spin();

    rclcpp::shutdown();
    return 0;
}