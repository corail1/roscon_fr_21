// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------
#include <stdexcept>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/int64.hpp"
#include "corail_core/corail_core.hpp"
#include "utils/rt_tools.hpp"

class publisher : public corail_core::RealTimeNode
{
    public:
    publisher(std::string name, std::chrono::microseconds p) : RealTimeNode(name)
    {
        pub_ = create_publisher<std_msgs::msg::Int64>("test_ros",1);
        timer_ = create_rt_timer("publisher",1,20,p,std::bind(&publisher::publish,this));
    }

    void publish()
    {
        auto msg = std_msgs::msg::Int64();
        msg.data = rclcpp::Node::now().nanoseconds(); // for ROS subscriber
        // msg.data = utils::now_ns(); // for corail subscriber
        pub_->publish(msg);
    }
    private:
    corail_core::PeriodicTask::SharedPtr timer_;
    rclcpp::Publisher<std_msgs::msg::Int64>::SharedPtr pub_;
};

int main(int argc, char * argv[])
{
    rclcpp::init(argc, argv);
    if(argc < 2)
    {
        throw std::invalid_argument("No period for timer");
    }
    auto p = std::chrono::milliseconds( atoi(argv[1]));
    corail_core::RealTimeExecutor exec("executor");
    auto node = std::make_shared<publisher>("publisher",p);
    exec.add_rt_node(node);
    exec.spin();
    rclcpp::shutdown();
    return 0;
}