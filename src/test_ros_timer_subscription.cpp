// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/int64.hpp"

#include "corail_tracing/tracing.hpp"
#include "utils/cpu.hpp"

using namespace std::chrono_literals;
using namespace std::placeholders;

class test_node : public rclcpp::Node
{
    public :
    test_node(std::string name) : Node(name)
    {
        timer_ = this->create_wall_timer(15ms,std::bind(&test_node::worker,this));
        sub_ = this->create_subscription<std_msgs::msg::Int64>("test_ros",10,std::bind(&test_node::subCallback,this,_1));
    }

    void worker()
    {
        corail::tracing::trace_begin("test_ros_timer_subscription","timer",(timer_->time_until_trigger()-20ms+rclcpp::Node::now()).nanoseconds(),0,rclcpp::Node::now().nanoseconds());
        utils::burn_cpu(7000);
        corail::tracing::trace_end("test_ros_timer_subscription","timer",(timer_->time_until_trigger()-20ms+rclcpp::Node::now()).nanoseconds(),0,rclcpp::Node::now().nanoseconds());
    }

    void subCallback(std_msgs::msg::Int64::SharedPtr msg)
    {
        corail::tracing::trace_begin("test_ros_timer_subscription","subscription",msg->data,0,rclcpp::Node::now().nanoseconds());
        utils::burn_cpu(7000);
        corail::tracing::trace_end("test_ros_timer_subscription","subscription",msg->data,0,rclcpp::Node::now().nanoseconds());
    }

    private :
    rclcpp::TimerBase::SharedPtr timer_;
    rclcpp::Subscription<std_msgs::msg::Int64>::SharedPtr sub_;
};

int main(int argc, char* argv[])
{
    rclcpp::init(argc,argv);
    
    rclcpp::executors::SingleThreadedExecutor exec;
    auto node = std::make_shared<test_node>("node");
    exec.add_node(node);

    exec.spin();

    rclcpp::shutdown();
    return 0;
}