// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/int64.hpp"

#include "corail_core/corail_core.hpp"
#include "utils/cpu.hpp"

using namespace std::chrono_literals;
using namespace std::placeholders;

class test_node : public corail_core::RealTimeNode
{
public :
    test_node(std::string name) : RealTimeNode(name)
    {
        timer_ = create_rt_timer("timer", 0, 10, 15ms, std::bind(&test_node::worker,this));
        sub_ = create_rt_subscription<std_msgs::msg::Int64>("sub", 0, 20, 1ms, 15ms, "test_ros", 1, std::bind(&test_node::subCallback, this, _1));
    };
    void worker()
{
    utils::burn_cpu(5000);
}

void subCallback(std_msgs::msg::Int64::SharedPtr msg)
{
    utils::burn_cpu(1000);
}

private :
corail_core::PeriodicTask::SharedPtr timer_;
corail_core::SubscriptionTask<std_msgs::msg::Int64>::SharedPtr sub_;
};



int main(int argc, char * argv[])
{
    rclcpp::init(argc,argv);

    corail_core::RealTimeExecutor exec("executor");
    auto node = std::make_shared<test_node>("test_node");
    exec.add_rt_node(node);

    exec.spin();

    rclcpp::shutdown();
    return 0;
}