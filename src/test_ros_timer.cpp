// Corail version 1.0, by Benoit Varillon and David Doose
// and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
// Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France

// This file is part of the Corail project.

// Corail is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Corail is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Corail.  If not, see <https://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------

#include <chrono>

#include "rclcpp/rclcpp.hpp"

#include "corail_tracing/tracing.hpp"
#include "utils/cpu.hpp"
#include "utils/rt_tools.hpp"

using namespace std::chrono_literals;

class test_node : public rclcpp::Node
{
    public :
    test_node(std::string name) : Node(name)
    {
        t2_ = this->create_wall_timer(50ms,std::bind(&test_node::worker2,this));
        t1_ = this->create_wall_timer(10ms,std::bind(&test_node::worker1,this));
    }

    void worker2()
    {
        corail::tracing::trace_begin("test_ros_timer","task2",(t2_->time_until_trigger()-50ms+rclcpp::Node::now()).nanoseconds(),0,rclcpp::Node::now().nanoseconds());
        utils::burn_cpu(13000);
        corail::tracing::trace_end("test_ros_timer","task2",(t2_->time_until_trigger()-50ms+rclcpp::Node::now()).nanoseconds(),0,rclcpp::Node::now().nanoseconds());
    }
    void worker1()
    {
        corail::tracing::trace_begin("test_ros_timer","task1",(t1_->time_until_trigger()-10ms+rclcpp::Node::now()).nanoseconds(),0,rclcpp::Node::now().nanoseconds());
        utils::burn_cpu(3000);
        corail::tracing::trace_end("test_ros_timer","task1",(t1_->time_until_trigger()-10ms+rclcpp::Node::now()).nanoseconds(),0,rclcpp::Node::now().nanoseconds());
    }

    private :
    rclcpp::TimerBase::SharedPtr t1_;
    rclcpp::TimerBase::SharedPtr t2_;
};

int main(int argc, char* argv[])
{
    rclcpp::init(argc,argv);
    utils::set_cpu(0);
    rclcpp::executors::SingleThreadedExecutor exec;
    auto node = std::make_shared<test_node>("node");
    exec.add_node(node);
    
    exec.spin();

    rclcpp::shutdown();
    return 0;
}