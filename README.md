# ROSConFR 2021
Corail version 1.0, by Benoit Varillon and David Doose
and Jean-Baptiste Chaudron and Charles Lesir-Cabaniols
Copyright 2021-2022 ISAE-Supaero, Université de Toulouse, France


This package contains the programs used to make the traces presented in the ROSConFR 21.

To produce correctly the traces for these programs you need to enable the good events by using :

for ROS2 programs :


    lttng enable-event corail_tracing:begin
    lttng enable-event corail_tracing:begin


for Corail programs :

    lttng enable-eventt corail_tracing:state_begin
    lttng enable-event corail_tracing:trigger_transition


See the LTTng documentation (https://lttng.org/) for more information about how to trace programs
